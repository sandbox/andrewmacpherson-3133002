<?php

namespace Drupal\migrate_prepare_row_message;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Event\MigratePrepareRowEvent;

/**
 * Provides helper methods to record messages during a MigratePrepareRowEvent.
 *
 * Use this in event subscriber classes which subscribe to the
 * `migrate_plus.prepare_row` event.
 *
 * @see Drupal\migrate_plus\Event\MigratePrepareRowEvent
 */
trait MigratePrepareRowMessageTrait {

  /**
   * Save a migration message for the current row.
   *
   * @param \Drupal\migrate_plus\Event\MigratePrepareRowEvent $event
   *   The event fired.
   * @param string $message
   *   The migration message to record.
   * @param int $level
   *   (optional) Message severity (defaults to MESSAGE_INFORMATIONAL).
   */
  protected function saveMessage(MigratePrepareRowEvent $event, $message, $level = MigrationInterface::MESSAGE_INFORMATIONAL) {
    /** @var Drupal\migrate\Plugin\MigrateIdMapInterface */
    $id_map = $event->getMigration()->getIdMap();

    /** @var \Drupal\migrate\Row */
    $row = $event->getRow();

    $id_map->saveMessage($row->getSourceIdValues(),
      $message,
      $level
    );
  }

}
